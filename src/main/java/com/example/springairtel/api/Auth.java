package com.example.springairtel.api;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.catalina.security.SecurityUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.stat.SessionStatistics;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.parser.JSONParser;;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import java.io.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import static com.example.springairtel.api.APIConstants.USER_AGENT;
import static javafx.scene.input.KeyCode.J;


public class Auth extends HttpServlet {
    private String CLIENT_URL = "", TEXT_PATH = "";
    private PostWithIgnoreSSLAirtelAuth postMinusThread;
    Properties prop = new Properties();
    OutputStream output = null;
    private Logger logger;

    // Error message provided when incorrect captcha is submitted
    final String ACCOUNT_SIGN_IN_BAD_CAPTCHA = "Sorry, the characters you entered did not "
            + "match those provided in the image. Please try again.";
    private BasicTextEncryptor textEncryptor;

    private String hiddenCaptchaStr = "";

    //private Cache accountsCache, statisticsCache;

    /**
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(FontImageGenerator.SECRET_KEY);

        CacheManager mgr = CacheManager.getInstance();
//        accountsCache = mgr.getCache(CacheVariables.CACHE_ACCOUNTS_BY_USERNAME);
//        statisticsCache = mgr.getCache(CacheVariables.CACHE_STATISTICS_BY_ACCOUNT);
        logger = Logger.getLogger(this.getClass());

    }
    public static String getMD5Hash(String input) {
        return getMD5Hash(input, null);
    }/*  w  ww   . d   e m   o  2   s  . c   o m */

    public static String getMD5Hash(String input, String salt) {
        MessageDigest digest;
        try {
            if (salt != null) {
                input = salt.trim() + input;
            }
            digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(input.getBytes());
            final byte[] hash = digest.digest();
            final StringBuilder result = new StringBuilder(hash.length);
            for (int i = 0; i < hash.length; i++) {
                result.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
            }
            return result.toString();
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "error";
        }
    }

    /**
     * @param request
     * @param response
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OutputStream out = response.getOutputStream();

        HttpSession session = request.getSession(false);

        if (session != null) {
            session.invalidate(); // This is in case the user had previously signed
            // in and his/her session is still active.
        }

        session = request.getSession(true);

        Account account = new Account();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        hiddenCaptchaStr = request.getParameter("captchaHidden");
        String captchaAnswer = request.getParameter("captchaAnswer").trim();

        Element element;
        if ((element = accountsCache.get(username)) != null) {
            account = (Account) element.getObjectValue();
        }

        if (account != null) {
            // Check that the system generated captcha and the user input for the captcha match
            if (validateCaptcha(gRecaptchaResponse) == false) {
                session.setAttribute(SessionConstants.ACCOUNT_SIGN_IN_ERROR_KEY, ACCOUNT_SIGN_IN_BAD_CAPTCHA);
                response.sendRedirect("index.jsp");

            } else {
                // Correct login
                if (StringUtils.equals(SecurityUtil.getMD5Hash(password), account.getLogpassword())) {
                    updateCache(account.getUuid());
                    session.setAttribute(SessionConstants.ACCOUNT_SIGN_IN_ACCOUNTUUID, account.getUuid());
                    session.setAttribute(SessionConstants.ACCOUNT_SIGN_IN_KEY, username);

                    session.setAttribute(SessionConstants.ACCOUNT_SIGN_IN_TIME,
                            String.valueOf(new Date().getTime()));

                    response.sendRedirect("account/inbox.jsp");

                    // Incorrect login, password not matching
                } else {
                    session.setAttribute(SessionConstants.ACCOUNT_SIGN_IN_ERROR_KEY,
                            SessionConstants.ACCOUNT_SIGN_IN_WRONG_PASSWORD);
                    response.sendRedirect("index.jsp");
                }

                response.setContentType("text/plain;charset=UTF-8");
                response.setDateHeader("Expires", new Date().getTime()); // Expiration
                // date
                response.setDateHeader("Date", new Date().getTime()); // Date and time
                // that the
                // message was
                // sent

                out.write(authentication(request).getBytes());
                out.flush();
                out.close();
            }
        }
    }

    public static boolean validateCaptcha(String gRecaptchaResponse) throws IOException {
        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            return false;
        }

        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String postParams = "secret=" + secret + "&response=" + gRecaptchaResponse;

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + postParams);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());

            //parse JSON response and return 'success' value
            JsonReader jsonReader = Gson.createReader(new StringReader(response.toString()));
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();

            return jsonObject.getBoolean("success");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

            /**
             *
             * @param request
             * @return
             * @throws IOException
             */
            private String authentication (HttpServletRequest request) throws IOException {
                // Account account = null;

                // joined json string
                String join = "";
                String token = "";
                JsonElement root = null, roots = null;
                JsonObject queryrequest = null;
                String responseobject = "", results2 = "", jsonResult = "", encodedString = "", toencode = "";

                // These represent parameters received over the network
                String username = "", sessionid = "", receiverquery = "";

                // Get all parameters
                List<String> lines = IOUtils.readLines(request.getReader());

                // used to format/join incoming JSon string
                join = StringUtils.join(lines.toArray(), "");

                // ###################################################################
                // instantiate the JSon
                // ###################################################################

                Gson g = new Gson();
                JsonArray jsonarray = new JsonArray();
                Map<String, String> expected = new HashMap<>();

                try {
                    // parse the JSon string
                    root = new JsonParser().parse(join);

                    username = root.getAsJsonObject().get("username").getAsString();
                    sessionid = root.getAsJsonObject().get("password").getAsString();
                    receiverquery = root.getAsJsonObject().get("receiverqueryurl").getAsString();

                } catch (Exception e) {

                    // expected.put("command_status",
                    // APIConstants.COMMANDSTATUS_INVALID_PARAMETERS);
                    expected.put("command_status", "COMMANDSTATUS_INVALID_PARAMETERS");
                    jsonResult = g.toJson(expected);

                    return jsonResult;
                }

                toencode = username + ":" + sessionid;

                // Encode data on your side using BASE64
                byte[] bytesEncoded = Base64.encodeBase64(toencode.getBytes());
                encodedString = new String(bytesEncoded);

                TEXT_PATH = PropertiesConfig.getConfigValue("AIRTELTEXT_PATH");
                CLIENT_URL = receiverquery;
                // CLIENT_URL =
                // "https://openapiuat.airtel.africa/auth/oauth2/token";
                postMinusThread = new PostWithIgnoreSSLAirtelAuth(CLIENT_URL, encodedString);

                try {

                    // capture the switch respoinse.
                    responseobject = postMinusThread.doGet();
                    System.out.println(responseobject);

                    // pass the returned json string
                    roots = new JsonParser().parse(responseobject);

                    // exctract a specific json element from the object(status_code)
                    token = roots.getAsJsonObject().get("access_token").getAsString();

                    expected.put("command_status", "SUCCESS_Airtel_SESSION");
                    jsonResult = g.toJson(expected);

                    try {
                        FileOutputStream writer = new FileOutputStream(TEXT_PATH);
                        writer.write(("").getBytes());
                        writer.close();

                        PrintWriter fileWriter = new PrintWriter(new FileOutputStream(TEXT_PATH, true));
                        fileWriter.println(token);
                        // out.println("file saved");
                        fileWriter.close();
                    } catch (IOException io) {
                        io.printStackTrace();
                    } finally {
                        if (output != null) {
                            try {
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                } catch (Exception e) {

                    // ================================================
                    // Missing fields in response from receiver system
                    // ================================================
                    expected.put("command_status", "COMMANDSTATUS_RECEIVER_SERVER_ERROR_PARAMETERS");
                    jsonResult = g.toJson(expected);

                    // return jsonResult;

                }

                logger.error(".....................................................");
                logger.error("AIRTEL QUERY GET AUTH FROM BRIDGE :" + encodedString + "\n");
                logger.error("AIRTEL QUERY GET AUTH  RESPONSE:" + jsonResult + "\n");
                logger.error(".....................................................");

                return jsonResult;

            }

            /**
             *
             * @param request
             * @param response
             * @throws ServletException
             *             , IOException
             */
//            @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        doPost(request, response);
//    }
//            private void updateCache(String accountuuid) {
//                SessionStatistics statistics = SessionStatisticsFactory.getSessionStatistics(accountuuid);
//
//                statisticsCache.put(new Element(accountuuid, statistics));
//            }
        }



