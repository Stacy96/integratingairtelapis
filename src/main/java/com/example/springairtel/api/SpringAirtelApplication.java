package com.example.springairtel.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAirtelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAirtelApplication.class, args);
	}

}
